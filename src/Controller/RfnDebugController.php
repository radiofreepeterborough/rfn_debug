<?php

namespace Drupal\rfn_debug\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for rfn_debug routes.
 */
class RfnDebugController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
